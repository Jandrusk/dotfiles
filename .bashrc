##################################################
# Setup Shell Prompt
##################################################
# export PS1=`echo -ne "0;34m\u@\h:\033 "`
# export PS1="\[\033[0;37m\]\342\224\214\342\224\200$([[ $? != 0 ]] && echo "[\[\033[0;31m\]\342\234\227\[\033[0;37m\]]\342\224\200")[\[\033[0;33m\]\u\[\033[0;37m\]@\[\033[0;96m\]\h\[\033[0;37m\]]\342\224\200[\[\033[0;32m\]\w\[\033[0;37m\]]\n\[\033[0;37m\]\342\224\224\342\224\200\342\224\200\342\225\274 \[\033[0m\]" 
# PS1='echo -ne "\033]0;\u"'
# Testing Aide
# eval `dircolors $HOME/.dir_colors`
# LS_COLORS="di=0;36:ex=0;35"
# export LS_COLORS
PS1="[\[\033[01;31m\]\u\[\033[00m\]@\h \[\033[01;34m\]\w\[\033[00m\]]\$ "
export PROMPT_DIRTRIM=2
source ~/src/scripts/vbox
source ~/src/scripts/torify
export HISTSIZE=1000000
export HISTFILESIZE=1000000
export COLORFGBG="default;default" 
shopt -s histappend # Preserve History File
export PROMPT_COMMAND="history -a; history -n"
export UQT_VM_TOOLS="$HOME/src/ubuntu/ubuntu-qa-tools/vm-tools"
export PYTHONPATH=$PYTHONPATH:~/src/diveintopython-5.4/py
export DEBFULLNAME="Justin R. Andrusk"
export DEBEMAIL="jandrusk@gmail.com"
export TERM=rxvt-unicode-256color
export BROWSER=/usr/bin/chromium-browser # For RTV to use Chromium instead of Firefox. 
export RTV_EDITOR=/usr/bin/nvim

# export GPG_AGENT_INFO=$HOME/.gnupg/S.gpg-agent
#--------------------------
# Bash Hacks
#--------------------------
alias refresh='source ~/.bashrc'
alias youtube-aud="youtube-dl --extract-audio --audio-format mp3"
#--------------------------
# Package Manager Aliases
#--------------------------
alias weechat="tmux new -s irc weechat"
export DEBIAN_FRONTEND=noninteractive
# alias weechat="screen weechat irc"
# alias attach="screen -r"
alias vms_off="sudo sed -i 's/^\*\/1/\#\/1/' /var/spool/cron/crontabs/jra"
alias vms_on="sudo sed -i 's/^\#\/1/\*\/1/' /var/spool/cron/crontabs/jra"
alias e="emacsclient -t"
alias ec="emacsclient -c"
alias rb="sudo reboot"
# alias vim="emacsclient -t"
# alias vi="emacsclient -t"
export EDITOR="vim"
# alias vi="vim"
# TMux aliases
alias tnw="tmux new -s "
alias tls="tmux ls"
alias tma="tmux a -t "
alias pf="sudo apt-cache search"
alias pp="apt-cache policy"
alias pi="sudo apt-get -y install"
alias pbd="sudo apt-get -y build\-dep"
alias pacupg="sudo pacman -Syu"
alias prm="sudo apt-get -y remove"
alias ppa="sudo add-apt-repository -y"
alias ipt="sudo iptables"
alias pam="sudo apt-get -y autoremove" #Autoremove
alias filebug="ubuntu-bug"
alias ka="killall"
alias sc="sudo service"
alias ps="ps -deaf"
alias cx="chmod 755"
alias f="locate"
alias down="sudo shutdown -h \"now\""
alias lpr="lpr -P Brother-HL-2140"
alias attach="screen -r"
alias edaemon="emacs-snapshot --daemon"
alias rpanel="xfce4-panel -r"
alias nv="nvim"
# Git Aliases
alias ga="git add"
alias gc="git commit -a"
alias gpo="git push origin master"



#-------------------
# Personnal Aliases
#-------------------
alias money="ledger -f ~/Documents/money/andrusk.dat"
alias py="python"
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias borg="ssh jra@192.168.1.133"
# -> Prevents accidentally clobbering files.
alias mkdir='mkdir -p'

alias h='history'
alias j='jobs -l'
alias r='rlogin'
alias which='type -all'
alias ..='cd ..'
alias path='echo -e ${PATH//:/\\n}'
alias print='/usr/bin/lp -o nobanner -d $LPDEST'   # Assumes LPDEST is defined
alias pjet='enscript -h -G -fCourier9 -d $LPDEST'  # Pretty-print using enscript
alias background='xv -root -quit -max -rmode 5'    # Put a picture in the background
alias du='du -kh'
alias df='df -kTh'

# The 'ls' family (this assumes you use the GNU ls)
alias la='ls -Al'               # show hidden files
alias ls='ls -hF --color' # add colors for filetype recognition
alias ll='ls -l --color'
alias lx='ls -lXB'              # sort by extension
alias lk='ls -lSr'              # sort by size
alias lc='ls -lcr' # sort by change time
alias lu='ls -lur' # sort by access time
alias lr='ls -lR'               # recursive ls
alias lt='ls -ltr'              # sort by date
alias lm='ls -al |more'         # pipe through 'more'
alias tree='tree -Csu' # nice alternative to 'ls'
# tailoring 'less'
# alias more='less'
PATH=$PATH:~/bin:/opt/vagrant/bin:/usr/lib/mutt/:/home/jra/src/ubuntu/ubuntu-qa-tools/vm-tools::~/src/scripts:/usr/local/bin:/sbin:/usr/lib/go-1.8/bin:/home/jra/games/t-engine4-linux64-1.5.5

# Sets the Mail Environment Variable
MAILDIR=~/Maildir
MAIL=$MAILDIR/INBOX && export MAIL

 # Load RVM source
 if [[ -s "/usr/local/rvm/scripts/rvm" ]]  ; then source "/usr/local/rvm/scripts/rvm" ; fi
 # Enable Tab Completion in RVM
 [[ -r /usr/local/rvm//scripts/completion ]] && source /usr/local/rvm/scripts/completion

export PERL_LOCAL_LIB_ROOT="/home/jra/perl5";
export PERL_MB_OPT="--install_base /home/jra/perl5";
export PERL_MM_OPT="INSTALL_BASE=/home/jra/perl5";
export PERL5LIB="/home/jra/perl5/lib/perl5/x86_64-linux-gnu-thread-multi:/home/jra/perl5/lib/perl5";
export PATH="/home/jra/perl5/bin:$PATH";

function startvms {
vboxmanage startvm "FreeBSD Bind 64" --type headless

}

function eload {
	kill -9 `ps | grep emacs | grep daemon | gawk '{print $2}'`
	emacs-snapshot --daemon
}

function pug {
    sudo apt-get update
    sudo apt-get -y upgrade --force-yes
    sudo apt-get -y dist-upgrade --force-yes
}

function pfix {
    sudo apt-get -y -f install
}

function pu {
	sudo apt-get update
}

function parm {
	sudo apt-get -y autoremove
}
function netrs {
	sudo service networking restart
}
function dus {
	du -k | sort -n | perl -ne 'if ( /^(\d+)\s+(.*$)/){$l=log($1+.1);$m=int($l/log(1024)); printf  ("%6.1f\t%s\t%25s  %s\n",($1/(2**(10*$m))),(("K","M","G","T","P")[$m]),"*"x (1.5*$l),$2);}'
}

PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting
[[ -s "$HOME/.rvm/scripts/rvm" ]] && . "$HOME/.rvm/scripts/rvm"  # Load RVM into a shell session *as a function*
[[ -s "$HOME/.rvm/scripts/rvm" ]] && . "$HOME/.rvm/scripts/rvm"  # Load RVM into a shell session *as a function*

# added by Anaconda 1.6.1 installer
# export PATH="/home/jra/anaconda/bin:$PATH"
# Test

# Add to the bottom of ~/.profile
# Check if this is being called from an interactive shell
# GPG Agent Check
#if `ps | grep gpg-agent 1>/dev/null`; then
#				echo "GPG Agent Running"
#else
#				echo "GPG Agent Not Running. Starting..."
#				gpg-agent --daemon
#fi

#if `ps | grep fetchmail 1>/dev/null`; then
#				echo "Fetchmail daemon running"
#else
#				echo "Fetcmail daemon not running. Starting..."
#				fetchmail
#fi

note() {
				  if [ ! -z "$1" ]; then
									    echo $(date +"%Y%m%d-%H%M%S") $@  >> $HOME/notes/TempNotes.wiki
											  else
																    echo $(date +"%Y%m%d-%H%M%S") "$(cat)"  >> $HOME/notes/TempNotes.wiki
																		  fi
															}
